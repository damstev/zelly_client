angular.module('zellyClient.devices')
        .factory('Brand', function ($resource, Config) {
            var url = Config.domainUrl + "devices/api/brands/:id";
            var Brand = $resource(url, {id: '@id', },
                    {
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        }
                    });
            return Brand;
        })
        .factory('Model', function ($resource, Config) {
            var url = Config.domainUrl + "devices/api/models/:id";
            var Model = $resource(url, {id: '@id', },
                    {
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        }
                    });
            return Model;
        })
        .factory('Device', function ($resource, Config) {
            var url = Config.domainUrl + "devices/api/devices/:id";
            var Device = $resource(url, {id: '@id', },
                    {
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        }
                    });
            return Device;
        });


