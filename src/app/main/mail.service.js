'use strict';

angular.module('zellyClient')
        .factory('MailService', function ($q, Config, $http) {
            var m = new mandrill.Mandrill(Config.mandrillApiKey);

            return{
                sendMail: function (params){
                    var deferred = $q.defer();
                    m.messages.send(params, function (res) {
                        deferred.resolve(res);                                                                                                                                                                                                    
                    }, function (err) {                                                                                                                                                                                                           
                        deferred.reject(err);                                                                                                                                                                                                     
                    });                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                    
                    return deferred.promise;  
                },
                sendMailSendgrid: function (params) {
                    var data = {
                      "personalizations": [
                        {
                          "to": params.message.to,
                          "subject": params.subject
                        }
                      ],
                      "from": {
                        "email": params.message.from_email,
                        "name": params.message.from_name
                      },
                      "content": [
                        {
                          "type": "text/html",
                          "value": params.message.html
                        }
                      ]
                    }
                    $http.defaults.headers.common['Authorization'] = "Bearer: " + Config.sengridApiKey;
                    return $http.post(Config.urlSengridApi, data);
                },
                prepareMail: function (model) {
                    var imageUrl = Config.urlImageBucket;
                    var html = "Un nuevo dispositivo fue agregado en la app: <br><br>";

                    html += '<h2>Datos Usuario</h2>';
                    html += 'Nombre : '+model.name+'<br>';
                    html += 'Correo : '+model.email+'<br>';
                    html += 'Teléfono : '+model.phone+'<br>';

                    html += '<h2>Datos Dispositivo</h2>';
                    html += 'Marca : '+model.brand+'<br>';
                    html += 'Modelo : '+model.model+'<br>';
                    html += 'Capacidad : '+model.capacity+'<br>';
                    html += 'Precio Minimo : '+model.minPrice+'<br>';
                    html += 'Precio Maximo : '+model.maxPrice+'<br>';

                    html += '<h2>Código Promocional: ' + model.promoCode + '</h2>';

                    html += '<h2>Fotos</h2>';
                    html += '<img src="'+imageUrl+model.imageToken+'picFile0">';
                    html += '<img src="'+imageUrl+model.imageToken+'picFile1">';
                    html += '<img src="'+imageUrl+model.imageToken+'picFile2">';

                    var params = {
                        "message": {
                            "from_email": "contactus@zelly.co",
                            "from_name": "Zelly Contact Us",
                            "to": [
                                {"email":"damstev.sonoflight@gmail.com"},
                                {"email":"soyzelly@gmail.com"},
                            ],
                            "subject": "Nuevo Dispositivo",
                            "html": html,
                            "auto_text": true
                        }
                    };

                    return this.sendMail(params);
                }
            };
        });
