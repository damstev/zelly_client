angular.module('zellyClient')
        .directive("limitTo", [function () {
                return {
                    restrict: "A",
                    link: function (scope, elem, attrs) {
                        var limit = parseInt(attrs.limitTo);
                        angular.element(elem).on("keydown", function (e) {
                            if(e.keyCode === 8 || e.keyCode === 46)
                                return true;
                            if (this.value.length == limit)
                                return false;
                        });
                    }
                }
            }]);