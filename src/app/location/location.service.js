angular.module('zellyClient.location')
        .factory('Country', function ($resource, Config) {
            var url = Config.domainUrl + "location/api/countries/:id";

            return $resource(url, {id: '@id' },
                    {
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });
        })
        .factory('Region', function ($resource, Config) {
            var url = Config.domainUrl + "location/api/regions/:id";

            return $resource(url, {id: '@id' },
                    {
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });
        })
        .factory('City', function ($resource, Config) {
            var url = Config.domainUrl + "location/api/cities/:id";

            return $resource(url, {id: '@id' },
                    {
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });
        });


