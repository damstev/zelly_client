'use strict';

angular.module('zellyClient', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap', 'zellyClient.steps', 'ui.select', 'ngStorage', 'angularFileUpload', 'zellyClient.devices', 'zellyClient.location', 'angular-md5'])
        .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
//            $locationProvider.html5Mode(true);

            // $stateProvider
            //         .state('home', {
            //             url: '/',
            //             templateUrl: 'app/main/main.html',
            //             controller: 'MainCtrl'
            //         });

            $urlRouterProvider.otherwise('/home');

        })
        .run(function($rootScope, $location, $window){
        	$window.ga('create', 'UA-60519310-1', 'auto');
        	$rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
			    window.scrollTo(0, 0);
			    $window.ga('send', 'pageview', $location.path());
			});
        });
