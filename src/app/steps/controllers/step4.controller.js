'use strict';

angular.module('zellyClient.steps')
        .controller('Step4Ctrl', function ($scope, $location, ObjRegister) {
            $scope.status = 1;
            $scope.humidity = 1;
            $scope.imei = 1;
            $scope.charger = 1;
            $scope.information = 1;
            $scope.imei_number = '';

            $scope.change = function () {
                console.log('change');
                ObjRegister.setData('dev_status', $scope.status);
                ObjRegister.setData('dev_humidity', $scope.humidity);
                ObjRegister.setData('dev_imei', $scope.imei);
                ObjRegister.setData('dev_charger', $scope.charger);
                ObjRegister.setData('dev_information', $scope.information);
                ObjRegister.setData('dev_imei_number', $scope.imei_number);
            };

            $scope.next = function () {
                $location.path('/step5');
            };

            $scope.continueDisabled = function () {
                return $scope.humidity == 1 || $scope.imei == 2 || $scope.information == 2 || $scope.imei_number.length < 15;
            };

            if (ObjRegister.dataExists('dev_status'))
                $scope.status = ObjRegister.getData('dev_status');

            if (ObjRegister.dataExists('dev_humidity'))
                $scope.humidity = ObjRegister.getData('dev_humidity');

            if (ObjRegister.dataExists('dev_imei'))
                $scope.imei = ObjRegister.getData('dev_imei');

            if (ObjRegister.dataExists('dev_charger'))
                $scope.charger = ObjRegister.getData('dev_charger');

            if (ObjRegister.dataExists('dev_information'))
                $scope.imei_number = ObjRegister.getData('dev_imei_number');


            ///Restrinjo a solo numeros el documento y el promotor
            $scope.$watch('imei_number', function (data) {
                if (data)
                    $scope.imei_number = data.replace(/[^\d]/g, '');
            });
        });
