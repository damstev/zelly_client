'use strict';

angular.module('zellyClient.steps')
.controller('ModelCtrl', function ($scope,  ObjRegister, $location, Brand, Model, $window) {

  var init = function(){
    if(!ObjRegister.dataExists('marca')){
      $location.path('/home');
    }

    var marca = ObjRegister.getData('marca');

    Model.query({brand: marca.id}).$promise.then(function(data){

      if(ObjRegister.dataExists('model')){
        var aux = ObjRegister.getData('model');
        var modelo  = _.find(data, { 'id' : aux.id});
        if(modelo)
          modelo.selected = true;
      }
      $scope.modelos = data;
    });
  };

  $scope.selectModel = function(model){
    console.log('model', model);
    
    model.extra=angular.fromJson(model.extra);
    mixpanel.track("Step2", {'Model': model.name});
    ObjRegister.setData('model', model);
    ObjRegister.delData('capacity');
    ObjRegister.delData('status');
    ObjRegister.delData('imageToken');
    ObjRegister.delData('picFile0');
    ObjRegister.delData('picFile1');
    ObjRegister.delData('picFile2');

    $location.path('/capacity');
  };

  init();

});
