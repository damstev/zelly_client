'use strict';

angular.module('zellyClient.steps')
        .controller('Step1Ctrl', function ($scope,  ObjRegister, $location, Brand, Model) {
            
            var fnGetBrands = function(){
                $scope.marcas = Brand.query();  
            };
            
            var fnGetModels = function (){
                $scope.modelos = Model.query({brand: $scope.marcas.selected.id});
            };
            
            $scope.marcas = [];
            $scope.modelos = [];
            
            $scope.selectMarca = function(item, model){ 
                ObjRegister.delData('modelo');
                ObjRegister.setData('marca', model);   
                fnGetModels();
            };
            
            $scope.selectModelo = function(item, model){ 
                ObjRegister.setData('modelo', model);
            };
            
            $scope.next = function(){ 
                $location.path('/step2');
            };
            
            $scope.continueDisabled = function(){ 
                if(ObjRegister.dataExists('marca') && ObjRegister.dataExists('modelo'))
                    return false;
                else
                    return true;
            };           
            
            
            fnGetBrands();
            
            if(ObjRegister.dataExists('marca'))
                $scope.marcas.selected = ObjRegister.getData('marca');
            
            if($scope.marcas.selected)
                fnGetModels();
            
            if(ObjRegister.dataExists('modelo'))
                $scope.modelos.selected = ObjRegister.getData('modelo');

            
        });
