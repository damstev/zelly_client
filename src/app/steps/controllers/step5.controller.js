'use strict';

angular.module('zellyClient.steps')
        .controller('Step5Ctrl', function ($scope, Region, City, ObjRegister, Device, MailService, $location) {

            var fnGetRegions = function () {
                $scope.regions = Region.query({country: 49});
            };

            var fnGetCities = function () {
                $scope.cities = City.query({region: $scope.regions.selected.id});
            };

            $scope.selectRegion = function (item, model) {
                ObjRegister.delData('city');
                ObjRegister.setData('region', model);
                fnGetCities();
            };

            $scope.selectCity = function (item, model) {
                ObjRegister.setData('city', model);
            };

            $scope.change = function () {
                console.log('change');
                ObjRegister.setData('user_first_name', $scope.user_first_name);
                ObjRegister.setData('user_last_name', $scope.user_last_name);
                ObjRegister.setData('user_email', $scope.user_email);
                ObjRegister.setData('user_address', $scope.user_address);
                ObjRegister.setData('user_phone', $scope.user_phone);
            };

            $scope.next = function () {
                var o = ObjRegister.getData();
                var objDevice = new Device();

                objDevice.dev_imei = o.dev_imei_number;
                objDevice.dev_status = o.dev_status;
                objDevice.dev_charger = o.dev_charger;
                objDevice.dev_img_id = o.imageToken;
                objDevice.user_first_name = o.user_first_name;
                objDevice.user_last_name = o.user_last_name;
                objDevice.user_email = o.user_email;
                objDevice.user_phone = o.user_phone;
                objDevice.user_country = 49;
                objDevice.user_region = o.region.id;
                objDevice.user_city = o.city.id;
                objDevice.user_address = o.user_address;

                objDevice.dev_brand = o.marca.id;
                objDevice.dev_model = o.modelo.id;

                console.log(objDevice);

                objDevice.$save().then(function (data) {
                    fnSendMail(o).then(function (data) {
                        ObjRegister.delData();
                        $location.path('/step6');
                    });
                }, function (data) {
                    alert('error');
                    console.log(data);
                });

            };

            var fnSendMail = function (data) {
                var status = 'Excelente';
                if (data.dev_status == 2)
                    status = 'Bueno';
                if (data.dev_status == 3)
                    status = 'Regular';

                var model = {
                    name: data.user_first_name + ' ' + data.user_last_name,
                    email: data.user_email,
                    phone: data.user_phone,
                    address: data.user_address + ', ' + data.region.name + '/' + data.city.name,
                    brand: data.marca.name,
                    model: data.modelo.name,
                    status: status,
                    charger: (data.dev_charger) ? 'Si' : 'No',
                    imei: data.dev_imei_number,
                    imageToken: data.imageToken,
                };
                return MailService.prepareMail(model);
            };

            $scope.continueDisabled = function () {
                return !$scope.user_first_name ||
                        !$scope.user_last_name ||
                        !$scope.user_email ||
//                        !$scope.user_address ||
                        !$scope.user_phone ||
                        !$scope.regions.selected ||
                        !$scope.cities.selected;
            };

            fnGetRegions();
            if (ObjRegister.dataExists('region'))
                $scope.regions.selected = ObjRegister.getData('region');

            if ($scope.regions.selected)
                fnGetCities();

            if (ObjRegister.dataExists('city'))
                $scope.cities.selected = ObjRegister.getData('city');

            if (ObjRegister.dataExists('user_first_name'))
                $scope.user_first_name = ObjRegister.getData('user_first_name');
            if (ObjRegister.dataExists('user_last_name'))
                $scope.user_last_name = ObjRegister.getData('user_last_name');
            if (ObjRegister.dataExists('user_email'))
                $scope.user_email = ObjRegister.getData('user_email');
            if (ObjRegister.dataExists('user_address'))
                $scope.user_address = ObjRegister.getData('user_address');
            if (ObjRegister.dataExists('user_phone'))
                $scope.user_phone = ObjRegister.getData('user_phone');


            ///Restrinjo a solo numeros el documento y el promotor
            $scope.$watch('user_phone', function (data) {
                if (data)
                    $scope.user_phone = data.replace(/[^\d]/g, '');
            });

        });
