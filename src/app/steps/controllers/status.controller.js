'use strict';

angular.module('zellyClient.steps')
.controller('StatusCtrl', function ($scope,  ObjRegister, $location, Brand, Model) {

  var init = function(){
    if(!ObjRegister.dataExists('model')){
      $location.path('/model');
    }

    if(ObjRegister.dataExists('status')){
      $scope.status = ObjRegister.getData('status');
    }
  };

  $scope.selectStatus = function(status){
    ObjRegister.setData('status', status);
    mixpanel.track("Step4", {'Status': status});
    ObjRegister.delData('imageToken');
    ObjRegister.delData('picFile0');
    ObjRegister.delData('picFile1');
    ObjRegister.delData('picFile2');
    $location.path('/pics');
  };

  init();

});
