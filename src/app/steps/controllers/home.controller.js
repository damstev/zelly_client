'use strict';

angular.module('zellyClient.steps')
        .controller('HomeCtrl', function ($scope,  ObjRegister, $location, Brand, Model) {

            var getPopularPhones = function () {
              $scope.popularPhones = [
                {
                  marca: {
                    name: 'IPHONE',
                    id: 6
                  },
                  model: {
                    name: '5',
                    id: 179
                  },
                  estimatePrice: 580000,
                  picFile: 'img-phone.png'
                },{
                  marca: {
                    name: 'SAMSUNG',
                    id: 2
                  },
                  model: {
                    name: 'GALAXY S5',
                    id: 183
                  },
                  estimatePrice: 570000,
                  picFile: 'img-samsung.png'
                },{
                  marca: {
                    name: 'MOTOROLA',
                    id: 4
                  },
                  model: {
                    name: 'MOTO G',
                    id: 163
                  },
                  estimatePrice: 180000,
                  picFile: 'img-motorola.png'
                },
              ];
            };

            var fnLookForBrandInBrands = function (brandId){
              var brand = _.find($scope.marcas, { 'id' : brandId});
              if (brand) return brand;
              else return false;
            };

            var fnGetBrands = function(){
                Brand.query().$promise.then(function(data){

                  if(ObjRegister.dataExists('marca')){
                    var aux = ObjRegister.getData('marca');
                    var marca  = _.find(data, { 'id' : aux.id});
                    marca.selected = true;
                  }

                  $scope.marcas = data;
                });
            };

            $scope.selectMarca = function(marca){
                mixpanel.track("Step1", {'Brand': marca.name});
                ObjRegister.setData('marca', marca);
                ObjRegister.delData('model');
                ObjRegister.delData('capacity');
                ObjRegister.delData('status');
                ObjRegister.delData('imageToken');
                ObjRegister.delData('picFile0');
                ObjRegister.delData('picFile1');
                ObjRegister.delData('picFile2');
                $location.path('/model');
            };

            $scope.selectPhone = function (phone){
              mixpanel.track("Step1", {'Brand': phone.marca.name});
              mixpanel.track("Step2", {'Model': phone.model.name});

              Model.get({id: phone.model.id})
                .$promise
                .then(function(data){
                  var auxBrand = fnLookForBrandInBrands(phone.marca.id);
                  var auxModel = data;
                  auxModel.extra = angular.fromJson(data.extra);

                  ObjRegister.setData('marca', auxBrand);
                  ObjRegister.setData('model', auxModel);
                  ObjRegister.delData('capacity');
                  ObjRegister.delData('status');
                  ObjRegister.delData('imageToken');
                  ObjRegister.delData('picFile0');
                  ObjRegister.delData('picFile1');
                  ObjRegister.delData('picFile2');

                  $location.path('/capacity');
              });
            };

            fnGetBrands();
            getPopularPhones();

        });


angular.module('zellyClient.steps')
  .filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
