'use strict';

angular.module('zellyClient.steps')
.controller('PersonalCtrl', function ($scope, ObjRegister, MailService, $location, $filter) {
  $scope.change = function () {
    console.log('change');
    ObjRegister.setData('user_name', $scope.user_name);
    ObjRegister.setData('user_email', $scope.user_email);
    ObjRegister.setData('user_phone', $scope.user_phone);
  };

  $scope.continueDisabled = function () {
    return !$scope.user_name ||
    !$scope.user_email ||
    !$scope.user_phone;
  };

  $scope.next = function () {
      var data = ObjRegister.getData();
      var model = {
          name: data.user_name,
          email: data.user_email,
          phone: data.user_phone,
          brand: data.marca.name,
          model: data.model.name,
          status: data.status,
          imageToken: data.imageToken,
          capacity : '',
          minPrice : '',
          maxPrice : '',
          promoCode : $scope.user_code
      };

      if(data.capacity){
        model.capacity = data.capacity.name;

        if(data.capacity.priceRange[data.status.toLowerCase()]){
          model.minPrice = data.capacity.priceRange[data.status.toLowerCase()].minimo;
          model.maxPrice = data.capacity.priceRange[data.status.toLowerCase()].maximo;
        }
      }

      priceRange: data.capacity.priceRange[data.status]
      MailService.prepareMail(model).then(function(data){
        ////Con las siguientes dos lineas se crea el perfil del usuario en mixpanel
        mixpanel.alias(model.email);
        mixpanel.people.set({
          "$email": model.email,
          "$first_name": model.name,

          "$phone": model.phone,
          "$created": new Date()
          //"u-City":ciudad
        });
        mixpanel.track("Step6", {'Model': "Registered User"});

        $location.path('/final');        
      })



  };

  /** Todo esto es para obtener los datos del localstorage si existen **/
  if(ObjRegister.dataExists('user_name')){
    $scope.user_name = ObjRegister.getData('user_name');
  }
  if(ObjRegister.dataExists('user_email')){
    $scope.user_email = ObjRegister.getData('user_email');
  }
  if(ObjRegister.dataExists('user_phone')){
    $scope.user_phone = ObjRegister.getData('user_phone');
  }

  if(ObjRegister.dataExists('model')){
    var model = ObjRegister.getData('model');
    console.log(model);
    if(ObjRegister.dataExists('status')){
      var status = ObjRegister.getData('status').toLowerCase();

      if(ObjRegister.dataExists('capacity')){
        var capacity = ObjRegister.getData('capacity');
        $scope.minPrice = $filter('currency')( capacity.priceRange[status].minimo, '$', 0);
        $scope.maxPrice = $filter('currency')( capacity.priceRange[status].maximo, '$', 0);
      }

      /*
      if(model.hasOwnProperty('extra') && model.extra && model.extra.hasOwnProperty('priceRange') && model.extra.priceRange && model.extra.priceRange.hasOwnProperty(status)){
        $scope.minPrice = $filter('currency')( model.extra.priceRange[status].minimo, '$', 0);
        $scope.maxPrice = $filter('currency')( model.extra.priceRange[status].maximo, '$', 0);
      }*/
    }
  }

  ///Restrinjo a solo numeros el numero telefonico
  $scope.$watch('user_phone', function (data) {
      if (data)
          $scope.user_phone = data.replace(/[^\d]/g, '');
  });
});
