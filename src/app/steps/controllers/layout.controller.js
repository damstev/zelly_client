'use strict';

angular.module('zellyClient.steps')
  .controller('LayoutController', function ($scope, $location) {
    var setStyles = function () {
      $scope.path = $location.path();
      if($scope.path !== '/home') $scope.center_logo_class = "text-center";
      else $scope.center_logo_class = "";
      console.log($scope.center_logo_class);
    };

    $scope.$on('$locationChangeStart', function (event, next, current){
      setStyles();
    });

    setStyles();
  });
