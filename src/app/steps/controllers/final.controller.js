'use strict';

angular.module('zellyClient.steps')
.controller('FinalCtrl', function ($scope,  ObjRegister, $location, $filter) {

	if(ObjRegister.dataExists('model')){
		var model = ObjRegister.getData('model');
		console.log(model);
		if(ObjRegister.dataExists('status')){
			var status = ObjRegister.getData('status').toLowerCase();

			if(ObjRegister.dataExists('capacity')){
				var capacity = ObjRegister.getData('capacity');
				$scope.minPrice = $filter('currency')( capacity.priceRange[status].minimo, '$', 0);
				$scope.maxPrice = $filter('currency')( capacity.priceRange[status].maximo, '$', 0);
			}
		}
	}

	$scope.next = function(){
		ObjRegister.delData();
		$location.path('/home');
	}
});
