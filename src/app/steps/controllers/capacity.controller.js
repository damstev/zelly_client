'use strict';

angular.module('zellyClient.steps')
.controller('CapacityCtrl', function ($scope,  ObjRegister, $location, Brand, Model, $window) {  

  var init = function(){
    if(!ObjRegister.dataExists('model')){
      $location.path('/model');
    }

    var model = ObjRegister.getData('model');
    console.log('model', model);

    if(model.hasOwnProperty('extra') && model.extra && model.extra.hasOwnProperty('capacities') && model.extra.capacities){
      $scope.capacities = model.extra.capacities;
      if(ObjRegister.dataExists('capacity')){
        var aux = ObjRegister.getData('capacity');
        var capacity  = _.find(model.extra.capacities, { 'name' : aux.name});
        if(capacity)
          capacity.selected = true;
      }
    }else{
      $location.path('/status');
    }
  };

  $scope.selectCapacity = function(capacity){
    mixpanel.track("Step3", {'Capacity': capacity.name});
    ObjRegister.setData('capacity', capacity);
    ObjRegister.delData('status');
    ObjRegister.delData('imageToken');
    ObjRegister.delData('picFile0');
    ObjRegister.delData('picFile1');
    ObjRegister.delData('picFile2');

    $location.path('/status');
  };

  init();

});
