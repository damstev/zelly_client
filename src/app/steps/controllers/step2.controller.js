'use strict';

angular.module('zellyClient.steps')
        .controller('Step2Ctrl', function ($scope, $location, ObjRegister) {

            $scope.acceptRequirements = false;
            $scope.acceptConditions = false;
            
            $scope.change = function(){
                ObjRegister.setData('acceptRequirements', $scope.acceptRequirements);
                ObjRegister.setData('acceptConditions', $scope.acceptConditions);
            };

            $scope.next = function () {
                $location.path('/step3');
            };

            $scope.continueDisabled = function () {
                return !$scope.acceptRequirements || !$scope.acceptConditions;
            };
            
            if(ObjRegister.dataExists('acceptRequirements'))
                $scope.acceptRequirements = ObjRegister.getData('acceptRequirements');
            
            if(ObjRegister.dataExists('acceptConditions'))
                $scope.acceptConditions = ObjRegister.getData('acceptConditions');

        });
