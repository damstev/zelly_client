'use strict';

angular.module('zellyClient.steps')
.controller('PicsCtrl', function ($scope, $upload, $timeout, ObjRegister, $location, $http, md5) {

  if(!ObjRegister.dataExists('model')){
    $location.path('/model');
  }

  $scope.usingFlash = FileAPI && FileAPI.upload != null;
  $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

  $scope.picFile0 = [];
  $scope.picFile1 = [];
  $scope.picFile2 = [];
  $scope.picFile3 = [];

  var imageToken = '';

  $scope.upload = function (file, name) {

    $upload.upload({
      url: 'https://s3-sa-east-1.amazonaws.com/zelly/',
      fields: {
        key: imageToken+name,
        AWSAccessKeyId: 'AKIAII4YZXUSI7VEB5OQ',
        acl: 'public-read',
        policy: 'ewogICJleHBpcmF0aW9uIjogIjIwMjAtMDEtMDFUMDA6MDA6MDBaIiwKICAiY29uZGl0aW9ucyI6IFsKICAgIHsiYnVja2V0IjogInplbGx5In0sCiAgICBbInN0YXJ0cy13aXRoIiwgIiRrZXkiLCAiIl0sCiAgICB7ImFjbCI6ICJwdWJsaWMtcmVhZCJ9LAogICAgWyJzdGFydHMtd2l0aCIsICIkQ29udGVudC1UeXBlIiwgIiJdLAogICAgWyJzdGFydHMtd2l0aCIsICIkZmlsZW5hbWUiLCAiIl0sCiAgICBbImNvbnRlbnQtbGVuZ3RoLXJhbmdlIiwgMCwgNTI0Mjg4MDAwXQogIF0KfQ==',
        signature: '7Ci/9jhc4fX32h6aKleU9SMerl8=',
        "Content-Type": file.type != '' ? file.type : 'application/octet-stream', // content type of the file (NotEmpty)
        filename: imageToken+name
      },
      file: file
    }).progress(function (evt) {
      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
    }).success(function (data, status, headers, config) {
      console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
      file.result = status;
      ObjRegister.setData(name, file);
    });
  };

  $scope.generateThumb = function (file, name) {
    //                console.log(file);
    if (file != null) {
      if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
        $timeout(function () {
          var fileReader = new FileReader();
          fileReader.readAsDataURL(file);
          fileReader.onload = function (e) {
            $timeout(function () {
              file.dataUrl = e.target.result;
            });
          };
          $scope.upload(file, name);
        });
      }
    }
  };

  $scope.next = function () {
    mixpanel.track("Step5", {'Fotos': "Uploaded pics"});
    $location.path('/personal');
  };

  $scope.continueDisabled = function () {
    return !$scope.picFile0 || !($scope.picFile0 && $scope.picFile0[0] && $scope.picFile0[0].result && $scope.picFile0[0].result === 204) ||
    !$scope.picFile1 || !($scope.picFile1 && $scope.picFile1[0] && $scope.picFile1[0].result && $scope.picFile1[0].result === 204) ||
    !$scope.picFile2 || !($scope.picFile2 && $scope.picFile2[0] && $scope.picFile2[0].result && $scope.picFile2[0].result === 204);
  };

  var fnGenerateImageToken = function () {
    var imageToken = Date.now() + user_ip;
  }

  if (ObjRegister.dataExists('picFile0'))
    $scope.picFile0[0] = ObjRegister.getData('picFile0');

  if (ObjRegister.dataExists('picFile1'))
    $scope.picFile1[0] = ObjRegister.getData('picFile1');

  if (ObjRegister.dataExists('picFile2'))
    $scope.picFile2[0] = ObjRegister.getData('picFile2');


  if (ObjRegister.dataExists('imageToken'))
    imageToken = ObjRegister.getData('imageToken');
  else
    //$http.get('http://api.hostip.info/get_json.php')
    //.success(function (data) {
    //  var user_ip = data.ip;
    //  imageToken = md5.createHash(Date.now() + user_ip);
    //  ObjRegister.setData('imageToken', imageToken);
    //})
    //.error(function (data) {
      imageToken = md5.createHash(Date.now() + '');
      ObjRegister.setData('imageToken', imageToken);
    //});

});
