angular.module('zellyClient.steps')
.directive('breadcrumb', function() {
  return {
    restrict: 'E',
    template: '<div class="conte-miga">\
      <div class="container">\
        <ul>\
          <li><a ui-sref="steps.home">{{marca.name | capitalize}}</a></li>\
          <li><a ui-sref="steps.model">{{modelo.name | capitalize}}</a></li>\
          <li><a ui-sref="steps.capacity">{{capacity.name}}</a></li>\
          <li><a ui-sref="steps.status">{{status}}</a></li>\
          <li><a ui-sref="steps.pics" ng-if="imageToken">Fotografias</a></li>\
        </ul>\
      </div>\
    </div>',
    controller: function($scope, $element, ObjRegister){
      if(ObjRegister.dataExists('marca')){
        $scope.marca = ObjRegister.getData('marca');
      }
      if(ObjRegister.dataExists('model')){
        $scope.modelo = ObjRegister.getData('model');
      }
      if(ObjRegister.dataExists('capacity')){
        $scope.capacity = ObjRegister.getData('capacity');
        console.log('$scope.capacity', $scope.capacity);
      }
      if(ObjRegister.dataExists('status')){
        $scope.status = ObjRegister.getData('status');
      }
      if (ObjRegister.dataExists('imageToken')){
        $scope.imageToken = ObjRegister.getData('imageToken');
      }
    }
  }
});
