'use strict';

angular.module('zellyClient.steps', ['zellyClient.devices'])
        .run(function($rootScope, $window){
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
                $window._fbq.push(['track', 'PixelInitialized', {}]);
                if('steps.final' === toState.name)
                    $window._fbq.push(['track', '6036008444539', {'value':'0.00','currency':'COP'}]);
            });
        })
        .constant('CLARO_SECRET', 'CL4R0')
        .config(function ($stateProvider, $resourceProvider) {
            $resourceProvider.defaults.stripTrailingSlashes = false;

            $stateProvider
                    .state('steps', {
                        abstract: true,
                        url: '',
                        templateUrl: 'app/steps/views/layout.html',
                        controller: 'LayoutController'
                    })
                    .state('steps.home', {
                        url: '/home',
                        templateUrl: 'app/steps/views/home.html',
                        controller: 'HomeCtrl'
                    })
                    .state('steps.model', {
                        url: '/model',
                        templateUrl: 'app/steps/views/model.html',
                        controller: 'ModelCtrl'
                    })
                    .state('steps.capacity', {
                        url: '/capacity',
                        templateUrl: 'app/steps/views/capacity.html',
                        controller: 'CapacityCtrl'
                    })
                    .state('steps.status', {
                        url: '/status',
                        templateUrl: 'app/steps/views/status.html',
                        controller: 'StatusCtrl'
                    })
                    .state('steps.pics', {
                        url: '/pics',
                        templateUrl: 'app/steps/views/pics.html',
                        controller: 'PicsCtrl'
                    })
                    .state('steps.personal', {
                        url: '/personal',
                        templateUrl: 'app/steps/views/personal.html',
                        controller: 'PersonalCtrl'
                    })
                    .state('steps.final', {
                        url: '/final',
                        templateUrl: 'app/steps/views/final.html',
                        controller: 'FinalCtrl'
                    })


                    .state('steps.step1', {
                        url: '/step1',
                        templateUrl: 'app/steps/views/step1.html',
                        controller: 'Step1Ctrl'
                    })
                    .state('steps.step2', {
                        url: '/step2',
                        templateUrl: 'app/steps/views/step2.html',
                        controller: 'Step2Ctrl'
                    })
                    .state('steps.step3', {
                        url: '/step3',
                        templateUrl: 'app/steps/views/step3.html',
                        controller: 'Step3Ctrl'
                    })
                    .state('steps.step4', {
                        url: '/step4',
                        templateUrl: 'app/steps/views/step4.html',
                        controller: 'Step4Ctrl'
                    })
                    .state('steps.step5', {
                        url: '/step5',
                        templateUrl: 'app/steps/views/step5.html',
                        controller: 'Step5Ctrl'
                    })
                    .state('steps.step6', {
                        url: '/step6',
                        templateUrl: 'app/steps/views/step6.html',
                        controller: 'Step6Ctrl'
                    })
                    .state('steps.step7', {
                        url: '/step7',
                        templateUrl: 'app/steps/views/step7.html',
                        controller: 'Step7Ctrl'
                    })
                    ;

        })
        ;
