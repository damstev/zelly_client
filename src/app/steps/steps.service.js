'use strict';

angular.module('zellyClient.steps')
        .factory('ObjRegister', function ($sessionStorage) {
            return {
                setData: function (key, value) {
                    if (!$sessionStorage.objRegister)
                        $sessionStorage.objRegister = {};
                    $sessionStorage.objRegister[key] = value;
                },
                getData: function (key) {
                    if (key)
                        return $sessionStorage.objRegister[key];
                    else
                        return $sessionStorage.objRegister;
                },
                dataExists: function (key) {
                    return $sessionStorage.objRegister && $sessionStorage.objRegister[key];
                },
                delData: function (key) {
                    if (key){
                        if ($sessionStorage.objRegister && $sessionStorage.objRegister[key])
                            delete $sessionStorage.objRegister[key];
                    }else{
                        $sessionStorage.$reset();
                    }
                }
            };
        });
              